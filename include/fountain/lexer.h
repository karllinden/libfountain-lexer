/*
 * This file is part of libfountain-lexer.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _FOUNTAIN_LEXER_H_
# define _FOUNTAIN_LEXER_H_

# include <stdio.h>

/* TODO: doxygen */

enum fountain_lexer_error
{
    FOUNTAIN_LEXER_OK = 0,
    FOUNTAIN_LEXER_ERROR_FILE,
    FOUNTAIN_LEXER_ERROR_INVALID,
    FOUNTAIN_LEXER_ERROR_MEMORY
};

enum fountain_token_type
{
    FOUNTAIN_TOKEN_ACTION,
    FOUNTAIN_TOKEN_CENTERED_TEXT_BEGIN,
    FOUNTAIN_TOKEN_CENTERED_TEXT_END,
    FOUNTAIN_TOKEN_CENTERED_TEXT_LEADER,
    FOUNTAIN_TOKEN_CENTERED_TEXT_TRAILER,
    FOUNTAIN_TOKEN_CHARACTER,
    FOUNTAIN_TOKEN_CHARACTER_EXTENSION,
    FOUNTAIN_TOKEN_CHARACTER_EXTENSION_BEGIN,
    FOUNTAIN_TOKEN_CHARACTER_EXTENSION_END,
    FOUNTAIN_TOKEN_CHARACTER_FORCING,
    FOUNTAIN_TOKEN_DUAL_DIALOGUE,
    FOUNTAIN_TOKEN_KEY,
    FOUNTAIN_TOKEN_SCENE_HEADING,
    FOUNTAIN_TOKEN_SCENE_HEADING_FORCER,
    FOUNTAIN_TOKEN_SCENE_NUMBER,
    FOUNTAIN_TOKEN_SCENE_NUMBER_BEGIN,
    FOUNTAIN_TOKEN_SCENE_NUMBER_END,
    FOUNTAIN_TOKEN_TEXT,
    FOUNTAIN_TOKEN_TEXT_BOLD_BEGIN,
    FOUNTAIN_TOKEN_TEXT_BOLD_END,
    FOUNTAIN_TOKEN_TEXT_ITALICS_BEGIN,
    FOUNTAIN_TOKEN_TEXT_ITALICS_END,
    FOUNTAIN_TOKEN_TEXT_NEWLINE,
    FOUNTAIN_TOKEN_TEXT_UNDERLINE_BEGIN,
    FOUNTAIN_TOKEN_TEXT_UNDERLINE_END,
    FOUNTAIN_TOKEN_VALUE_BEGIN,
    FOUNTAIN_TOKEN_VALUE_END
};

struct fountain_token
{
    enum fountain_token_type type;
    unsigned line;
    unsigned col_begin;
    unsigned col_end;
    const char * begin;
};

struct fountain_line
{
    const char * begin;
    unsigned len;
};

/**
 * Lexer callback that is called for each token in the input.
 *
 * Since token emission is called often and deeply in the lexer, having
 * a return value to show whether or not to continue would be awkward
 * and inefficient. Instead the lexer is optimized by not breaking (less
 * branching required). If no more tokens are desired, set the callback
 * to a noop.
 */
typedef void (fountain_token_cb_t)(
        const struct fountain_token *,
        void *);

enum fountain_lexer_source
{
    FOUNTAIN_LEXER_SOURCE_INVALID = 0,
    FOUNTAIN_LEXER_SOURCE_FILE,
    FOUNTAIN_LEXER_SOURCE_STRING,
    FOUNTAIN_LEXER_SOURCE_LINES
};

enum fountain_lexer_report
{
    FOUNTAIN_LEXER_REPORT_INVALID = 0,
    FOUNTAIN_LEXER_REPORT_CALLBACK,
    FOUNTAIN_LEXER_REPORT_TOKENS
};

struct fountain_lexer
{
    enum fountain_lexer_source source;
    union {
        FILE * file;
        const char * string;
        struct {
            void * lines;
            size_t line_size;
            size_t n_lines;
        };
    };

    enum fountain_lexer_report report;
    union {
        struct {
            fountain_token_cb_t * callback;
            void * data;
        };
        struct {
            struct fountain_token * tokens;
            size_t n_alloc_tokens;
            size_t n_tokens;
        };
    };

    enum fountain_lexer_error error;
};

const char * fountain_token_strtype(
        const enum fountain_token_type type);

void fountain_lexer_init(struct fountain_lexer * lexer);
void fountain_lexer_deinit(struct fountain_lexer * lexer);

void fountain_lexer_file(
        struct fountain_lexer * lexer,
        FILE * file);

void fountain_lexer_string(
        struct fountain_lexer * lexer,
        const char * string);

void fountain_lexer_lines(
        struct fountain_lexer * lexer,
        void * lines,
        size_t line_size,
        size_t n_lines);

void fountain_lexer_callback(
        struct fountain_lexer * lexer,
        fountain_token_cb_t * callback,
        void * data);

void fountain_lexer_for_tokens(
        struct fountain_lexer * lexer,
        struct fountain_token * tokens,
        size_t n_alloc_tokens);

void fountain_lexer_lex(struct fountain_lexer * lexer);

#endif /* !_FOUNTAIN_LEXER_H_ */
