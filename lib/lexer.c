/*
 * This file is part of libfountain-lexer.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <fountain/lexer.h>

#define TOKENS_CHUNK_SIZE 64
#define FILE_LINE_CHUNK_SIZE 32

#define TO_COL(str, line) ((unsigned) (str - line->begin) + 1)

struct reader_class
{
    void (*init)(void * reader, struct fountain_lexer * lexer);
    void (*deinit)(void * reader);
    const struct fountain_line * (*acq)(void * reader);
    void (*rel)(void * reader, const struct fountain_line * line);
};

struct file_line
{
    const char * begin;
    unsigned len;
    unsigned siz;
    struct file_line * next;
    char buf[];
};

struct file_reader
{
    struct fountain_lexer * lexer;
    FILE * file;
    struct file_line * rel_lines;
};

struct priv
{
    union {
        struct file_reader file;
    } reader;

    struct fountain_lexer * lexer;
    const struct reader_class * class;

    /*
     * Storage for tokens that should be emitted. Declared here to be
     * stored only once on the stack, since tokens are emitted
     * sequentially.
     */
    struct fountain_token token;

    const struct fountain_line * unread_line;
    unsigned line;

    struct fountain_token * tokens;
    size_t n_tokens;
    size_t n_alloc_tokens;
};

static void file_reader_init_vp(void * reader,
        struct fountain_lexer * lexer);
static void file_reader_deinit_vp(void * reader);
static const struct fountain_line * file_reader_acq_vp(void * reader);
static void file_reader_rel_vp(void * reader,
        const struct fountain_line * line);

static const struct reader_class file_reader_class = {
    &file_reader_init_vp,
    &file_reader_deinit_vp,
    &file_reader_acq_vp,
    &file_reader_rel_vp
};

enum flag {
    NONE = -1,
    BOLD = 0,
    ITALICS = 1,
    UNDERLINE = 2
};

struct flag_mapping
{
    const char * (*find_leading)(const char *, const char *);
    const char * (*find_trailing)(const char *, const char *);
    enum fountain_token_type begin;
    enum fountain_token_type end;
    unsigned token_size;
};

static const char * find_leading_double_asterisk(const char * begin,
        const char * end);
static const char * find_trailing_double_asterisk(const char * begin,
        const char * end);
static const char * find_leading_single_asterisk(const char * begin,
        const char * end);
static const char * find_trailing_single_asterisk(const char * begin,
        const char * end);
static const char * find_leading_underscore(const char * begin,
        const char * end);
static const char * find_trailing_underscore(const char * begin,
        const char * end);

const struct flag_mapping flag_mappings[] = {
    {
        &find_leading_double_asterisk,
        &find_trailing_double_asterisk,
        FOUNTAIN_TOKEN_TEXT_BOLD_BEGIN,
        FOUNTAIN_TOKEN_TEXT_BOLD_END,
        2
    },
    {
        &find_leading_single_asterisk,
        &find_trailing_single_asterisk,
        FOUNTAIN_TOKEN_TEXT_ITALICS_BEGIN,
        FOUNTAIN_TOKEN_TEXT_ITALICS_END,
        1
    },
    {
        &find_leading_underscore,
        &find_trailing_underscore,
        FOUNTAIN_TOKEN_TEXT_UNDERLINE_BEGIN,
        FOUNTAIN_TOKEN_TEXT_UNDERLINE_END,
        1
    }
};

struct marker
{
    const enum flag flag;
    const char * pos;
    struct marker * next;
};

const char *
fountain_token_strtype(const enum fountain_token_type type)
{
#define CASE(TYPE) case FOUNTAIN_TOKEN_ ## TYPE: return #TYPE
    switch (type) {
        CASE(ACTION);
        CASE(CENTERED_TEXT_BEGIN);
        CASE(CENTERED_TEXT_END);
        CASE(CENTERED_TEXT_LEADER);
        CASE(CENTERED_TEXT_TRAILER);
        CASE(CHARACTER);
        CASE(CHARACTER_EXTENSION);
        CASE(CHARACTER_EXTENSION_BEGIN);
        CASE(CHARACTER_EXTENSION_END);
        CASE(CHARACTER_FORCING);
        CASE(DUAL_DIALOGUE);
        CASE(KEY);
        CASE(SCENE_HEADING);
        CASE(SCENE_HEADING_FORCER);
        CASE(SCENE_NUMBER);
        CASE(SCENE_NUMBER_BEGIN);
        CASE(SCENE_NUMBER_END);
        CASE(TEXT);
        CASE(TEXT_BOLD_BEGIN);
        CASE(TEXT_BOLD_END);
        CASE(TEXT_ITALICS_BEGIN);
        CASE(TEXT_ITALICS_END);
        CASE(TEXT_NEWLINE);
        CASE(TEXT_UNDERLINE_BEGIN);
        CASE(TEXT_UNDERLINE_END);
        CASE(VALUE_BEGIN);
        CASE(VALUE_END);
        default:
            return "UNKNOWN";
    }
#undef CASE
}

void
fountain_lexer_init(struct fountain_lexer * lexer)
{
    memset(lexer, 0, sizeof(*lexer));
}

void
fountain_lexer_deinit(struct fountain_lexer * lexer)
{
    if (lexer->report == FOUNTAIN_LEXER_REPORT_TOKENS) {
        free(lexer->tokens);
    }
}

void
fountain_lexer_file(struct fountain_lexer * lexer, FILE * file)
{
    lexer->source = FOUNTAIN_LEXER_SOURCE_FILE;
    lexer->file = file;
}

void
fountain_lexer_string(struct fountain_lexer * lexer,
        const char * string)
{
    lexer->source = FOUNTAIN_LEXER_SOURCE_STRING;
    lexer->string = string;
}

void
fountain_lexer_lines(struct fountain_lexer * lexer,
        void * lines,
        size_t line_size,
        size_t n_lines)
{
    lexer->source = FOUNTAIN_LEXER_SOURCE_LINES;
    lexer->lines = lines;
    lexer->line_size = line_size;
    lexer->n_lines = n_lines;
}

void
fountain_lexer_callback(struct fountain_lexer * lexer,
        fountain_token_cb_t * callback,
        void * data)
{
    lexer->report = FOUNTAIN_LEXER_REPORT_CALLBACK;
    lexer->callback = callback;
    lexer->data = data;
}

void
fountain_lexer_for_tokens(struct fountain_lexer * lexer,
        struct fountain_token * tokens,
        size_t n_alloc_tokens)
{
    lexer->report = FOUNTAIN_LEXER_REPORT_TOKENS;
    lexer->tokens = tokens;
    lexer->n_alloc_tokens = n_alloc_tokens;
}

static void
validate_lexer(struct fountain_lexer * lexer)
{
    if (lexer->source == FOUNTAIN_LEXER_SOURCE_INVALID
            || lexer->report == FOUNTAIN_LEXER_REPORT_INVALID)
    {
        goto invalid;
    }

    /*
     * A file cannot be parsed for a token array since it will result in
     * pointers to freed memory due to the begin pointer in struct
     * fountain_token.
     */
    if (lexer->source == FOUNTAIN_LEXER_SOURCE_FILE
            && lexer->report == FOUNTAIN_LEXER_REPORT_TOKENS)
    {
        goto invalid;
    }

    if (0) {
invalid:
        lexer->error = FOUNTAIN_LEXER_ERROR_INVALID;
    }
}

static const struct fountain_line *
acq_line(struct priv * priv)
{
    return (*priv->class->acq)(&priv->reader);
}

static void
rel_line(struct priv * priv, const struct fountain_line * line)
{
    (*priv->class->rel)(&priv->reader, line);
}

static const struct fountain_line *
read_line(struct priv * priv)
{
    const struct fountain_line * line;

    priv->line++;

    line = priv->unread_line;
    if (line) {
        priv->unread_line = NULL;
        return line;
    }

    return acq_line(priv);
}

static void
unread_line(struct priv * priv, const struct fountain_line * line)
{
    assert(!priv->unread_line);
    priv->unread_line = line;
    priv->line--;
}

static void
init_priv(struct priv * priv, struct fountain_lexer * lexer)
{
    memset(priv, 0, sizeof(*priv));
    priv->lexer = lexer;
}

static void
deinit_priv(struct priv * priv)
{
    if (priv->unread_line) {
        rel_line(priv, priv->unread_line);
    }
    if (priv->class && priv->class->deinit) {
        (*priv->class->deinit)(&priv->reader);
    }
}

static int
file_line_alloc_chunk(struct file_line ** linep,
        struct fountain_lexer * lexer)
{
    unsigned siz = (*linep)->siz + FILE_LINE_CHUNK_SIZE;
    struct file_line * line = realloc(*linep, sizeof(*line) + siz);
    if (!line) {
        lexer->error = FOUNTAIN_LEXER_ERROR_MEMORY;
        return 1;
    }

    line->begin = line->buf;
    line->siz = siz;

    *linep = line;
    return 0;
}

static struct file_line *
file_line_new(struct fountain_lexer * lexer)
{
    struct file_line * line;

    line = malloc(sizeof(*line) + FILE_LINE_CHUNK_SIZE);
    if (!line) {
        lexer->error = FOUNTAIN_LEXER_ERROR_MEMORY;
        return NULL;
    }

    line->begin = line->buf;
    line->len = 0;
    line->siz = FILE_LINE_CHUNK_SIZE;
    line->next = NULL;

    return line;
}

static void
file_line_destroy(struct file_line * line)
{
    free(line);
}

static void
file_reader_init(struct file_reader * reader,
        struct fountain_lexer * lexer)
{
    reader->lexer = lexer;
    reader->file = lexer->file;
    reader->rel_lines = NULL;
}

static void
file_reader_deinit(struct file_reader * reader)
{
    struct file_line * line = reader->rel_lines;
    struct file_line * next;
    while (line) {
        next = line->next;
        file_line_destroy(line);
        line = next;
    }
}

static struct file_line *
file_reader_get_line(struct file_reader * reader)
{
    struct file_line * line = reader->rel_lines;
    if (line != NULL) {
        reader->rel_lines = line->next;
        return line;
    }
    return file_line_new(reader->lexer);
}

static void
file_reader_rel(struct file_reader * reader, struct file_line * line)
{
    line->next = reader->rel_lines;
    reader->rel_lines = line;
}

static struct file_line *
file_reader_acq(struct file_reader * reader)
{
    FILE * file = reader->file;
    struct file_line * line;
    char * buf;
    char * end;
    int c;
    int ret;

    line = file_reader_get_line(reader);
    if (!line) {
        return NULL;
    }

    buf = line->buf;
    end = buf + line->siz;

    c = fgetc(file);
    if (c == EOF) {
        if (ferror(file)) {
            goto error_ferror;
        } else {
            /* EOF and no error. End here, but do not set any error. */
            goto end;
        }
    } else if (c == '\n') {
        *buf = '\0';
        line->len = 0;
        return line;
    }
    *buf = (char)c;
    buf++;

    for (;;) {
        for (; buf < end; ++buf) {
            c = fgetc(file);
            if (c == EOF && ferror(file)) {
                goto error_ferror;
            } else if (c == EOF || c == '\n') {
                *buf = '\0';
                line->len = (unsigned)(buf - line->buf);
                return line;
            }

            *buf = (char)c;
        }

        unsigned len = (unsigned)(buf - line->buf);
        ret = file_line_alloc_chunk(&line, reader->lexer);
        if (ret) {
            /*
             * The function file_line_alloc_chunk should have set the
             * error in the lexer struct, so goto end.
             */
            goto end;
        }
        buf = line->buf + len;
        end = line->buf + line->siz;
    }

error_ferror:
    __attribute__((cold));
    reader->lexer->error = FOUNTAIN_LEXER_ERROR_FILE;
end:
    file_reader_rel(reader, line);
    return NULL;
}

static void
file_reader_init_vp(void * reader, struct fountain_lexer * lexer)
{
    file_reader_init(reader, lexer);
}

static void
file_reader_deinit_vp(void * reader)
{
    file_reader_deinit(reader);
}

static const struct fountain_line *
file_reader_acq_vp(void * reader)
{
    return (const struct fountain_line *)file_reader_acq(reader);
}

static void
file_reader_rel_vp(void * reader, const struct fountain_line * line)
{
    file_reader_rel(reader, (struct file_line *)line);
}

static void
set_type(struct priv * priv, enum fountain_token_type type)
{
    priv->token.type = type;
}

static void
set_line(struct priv * priv)
{
    priv->token.line = priv->line;
}

static void
set_begin(struct priv * priv, const struct fountain_line * line,
        const char * begin)
{
    priv->token.col_begin = 1 + begin - line->begin;
    priv->token.begin = begin;
}

static void
set_len(struct priv * priv, unsigned len)
{
    priv->token.col_end = priv->token.col_begin + len;
}

static void
set_end(struct priv * priv, const struct fountain_line * line,
        const char * end)
{
    priv->token.col_end = 1 + end - line->begin;
}

static void
emit(struct priv * priv)
{
    struct fountain_lexer * lexer = priv->lexer;
    (*lexer->callback)(&priv->token, lexer->data);
}

static const char * __attribute__((pure))
find_leading_underscore(const char * begin, const char * end)
{
    if (*begin == '_') {
        return begin;
    }

    const char * c = begin;
    do {
        c++;
        c = memchr(c, '_', end - c);
    } while (c && (*(c-1) == '\\' || *(c+1) == ' '));

    return c;
}

static const char * __attribute__((pure))
find_trailing_underscore(const char * begin, const char * end)
{
    if (*begin == '_') {
        return begin;
    }

    const char * c = begin;
    do {
        c++;
        c = memchr(c, '_', end - c);
    } while (c && (*(c-1) == '\\' || *(c-1) == ' '));

    return c;
}

static const char * __attribute__((pure))
find_leading_single_asterisk(const char * begin, const char * end)
{
    const char * c = begin;

    if (begin[0] == '*') {
        if (begin[1] != '*') {
            return begin;
        } else {
            c += 2;
        }
    }

    for (;;) {
        c = memchr(c, '*', end - c);
        if (!c) {
            return NULL;
        } else if (*(c-1) == '\\' || *(c+1) == ' ') {
            c++;
        } else if (*(c+1) == '*') {
            c += 2;
        }
    }
}

static const char * __attribute__((pure))
find_trailing_single_asterisk(const char * begin, const char * end)
{
    const char * c = begin;

    if (begin[0] == '*') {
        if (begin[1] != '*') {
            return begin;
        } else {
            c += 2;
        }
    }

    for (;;) {
        c = memchr(c, '*', end - c);
        if (!c) {
            return NULL;
        } else if (*(c-1) == '\\' || *(c-1) == ' ') {
            c++;
        } else if (*(c+1) == '*') {
            c += 2;
        }
    }
}

static const char * __attribute__((pure))
find_leading_double_asterisk(const char * begin, const char * end)
{
    if (begin[0] == '*' && begin[1] == '*') {
        return begin;
    }

    const char * c = begin;
    do {
        c++;
        c = memchr(c, '*', end - c);
    } while (c && (*(c-1) == '\\' || *(c+1) != '*' || *(c+2) == ' '));

    return c;
}

static const char * __attribute__((pure))
find_trailing_double_asterisk(const char * begin, const char * end)
{
    if (begin[0] == '*' && begin[1] == '*') {
        return begin;
    }

    const char * c = begin;
    do {
        c++;
        c = memchr(c, '*', end - c);
    } while (c && (*(c-1) == '\\' || *(c-1) == ' ' || *(c+1) != '*'));

    return c;
}

static void
insert_marker(struct marker ** markers, struct marker * marker)
{
    struct marker * next = *markers;

    /*
     * There is no need to do NULL checks in this function since the
     * marker should not be past the end marker which is the last
     * element of the linked list.
     */
    if (marker->pos > next->pos) {
        struct marker * prev;
        do {
            prev = next;
            next = next->next;
        } while (marker->pos > next->pos);
        prev->next = marker;
    } else {
        *markers = marker;
    }

    marker->next = next;
}

static void
lex_text_with_end(struct priv * priv, const struct fountain_line * line,
        const char * begin, const char * end)
{
    const char * ch = begin;
    struct marker format_markers[] = {
        {
            BOLD,
            NULL,
            NULL
        },
        {
            ITALICS,
            NULL,
            NULL
        },
        {
            UNDERLINE,
            NULL,
            NULL
        }
    };
    struct marker end_marker = {
        NONE,
        end,
        NULL
    };
    struct marker * markers = &end_marker;
    int state = 0;

    for (enum flag i = 0; i < 3; ++i) {
        struct marker * marker = format_markers + i;
        marker->pos = (*flag_mappings[i].find_leading)(begin, end);
        if (marker->pos) {
            insert_marker(&markers, marker);
        }
    }

    for (;;) {
        struct marker * marker;
        const struct flag_mapping * mapping;
        const char * pos;
        int flag;
        int is_begin;
        const char * (*find)(const char *, const char *);

        marker = markers;
        markers = marker->next;

        /* Emit text up to marker. */
        if (ch < marker->pos) {
            set_type(priv, FOUNTAIN_TOKEN_TEXT);
            set_line(priv);
            set_begin(priv, line, ch);
            set_end(priv, line, marker->pos);
            emit(priv);
            ch = marker->pos;
        }

        /*
         * If text has been emitted to the end marker, nothing more
         * needs to be done.
         */
        flag = marker->flag;
        if (flag == NONE) {
            return;
        }

        is_begin = !(state & (1 << flag));
        mapping = flag_mappings + flag;

        /*
         * Find the next marker. If the current marker should be a begin
         * token, but no end token can be found then there should be no
         * begin token emitted.
         */
        pos = marker->pos + mapping->token_size;
        if (pos < end) {
            find = is_begin
                ? mapping->find_trailing
                : mapping->find_leading;
            marker->pos = (*find)(pos, end);
            if (marker->pos) {
                insert_marker(&markers, marker);
            }
        } else {
            marker->pos = NULL;
        }

        /* Emit token and update state. */
        if (!is_begin || marker->pos) {
            set_type(priv, is_begin ? mapping->begin : mapping->end);
            set_line(priv);
            set_begin(priv, line, ch);
            set_len(priv, mapping->token_size);
            emit(priv);

            ch += mapping->token_size;

            state ^= (1 << flag);
        }
    }
}

static void
lex_text(struct priv * priv, const struct fountain_line * line,
        const char * begin)
{
    const char * end = line->begin + line->len;
    lex_text_with_end(priv, line, begin, end);
}

static int
is_value_line(const struct fountain_line * line)
{
    /*
     * From the syntax specification:
     *
     * "Indenting is 3 or more spaces, or a tab."
     */
    const char * begin = line->begin;
    return (begin[0] == '\t'
            || (begin[0] == ' ' && begin[1] == ' ' && begin[2] == ' '));
}

static void
lex_value(struct priv * priv, const struct fountain_line * line,
        const char * begin)
{
    const struct fountain_lexer * lexer = priv->lexer;
    const char * end;
    const char * newline;
    unsigned col;
    bool should_release = false;

    end = line->begin + line->len;
    while (isspace(*begin) && begin < end) {
        ++begin;
    }
    if (begin == end) {
        line = read_line(priv);
        if (!line) {
            return;
        } else if (!is_value_line(line)) {
            unread_line(priv, line);
            return;
        }
        should_release = true;

        begin = line->begin;
        end = begin + line->len;

        while (isspace(*begin) && begin < end) {
            ++begin;
        }
    }

    set_type(priv, FOUNTAIN_TOKEN_VALUE_BEGIN);
    set_line(priv);
    set_begin(priv, line, begin);
    set_len(priv, 0);
    emit(priv);

    lex_text(priv, line, begin);

    newline = line->begin + line->len;
    col = line->len + 1;

    if (should_release) {
        rel_line(priv, line);
    }

    while ((line = read_line(priv))
            && is_value_line(line)
            && !lexer->error)
    {
        /*
         * The newline belongs to the previous line.
         */
        set_type(priv, FOUNTAIN_TOKEN_TEXT_NEWLINE);
        priv->token.line = priv->line - 1;
        priv->token.col_begin = col;
        priv->token.col_end = col;
        priv->token.begin = newline;
        emit(priv);

        begin = line->begin;
        end = begin + line->len;
        while (isspace(*begin) && begin < end) {
            ++begin;
        }

        lex_text(priv, line, begin);

        newline = line->begin + line->len;
        col = line->len + 1;

        rel_line(priv, line);
    }
    if (line) {
        unread_line(priv, line);
    }

    if (!lexer->error) {
        set_type(priv, FOUNTAIN_TOKEN_VALUE_END);
        priv->token.line = priv->line;
        priv->token.col_begin = col;
        priv->token.col_end = col;
        priv->token.begin = newline;
        emit(priv);
    }
}

static void
lex_key(struct priv * priv, const struct fountain_line * line,
        const char * colon)
{
    set_type(priv, FOUNTAIN_TOKEN_KEY);
    set_line(priv);
    set_begin(priv, line, line->begin);
    set_end(priv, line, colon);
    emit(priv);

    lex_value(priv, line, colon + 1);
}

static void
lex_title_page(struct priv * priv)
{
    struct fountain_lexer * lexer = priv->lexer;
    const struct fountain_line * line;
    const char * colon;
    for (line = read_line(priv);
            line
                && (colon = memchr(line->begin, ':', line->len))
                && !lexer->error;
            line = read_line(priv))
    {
        lex_key(priv, line, colon);
        rel_line(priv, line);
    }
    if (line) {
        unread_line(priv, line);
    }
}

static const char *
skip_spaces(const char * str)
{
    return str + strspn(str, " \t");
}

static int
line_is_empty(const struct fountain_line * line)
{
    return *line->begin == '\0' || *line->begin == '\n';
}

static void
lex_centered_text(struct priv * priv, const struct fountain_line * line,
        const char * first, const char * last)
{
    const char * begin;
    const char * end;

    set_type(priv, FOUNTAIN_TOKEN_CENTERED_TEXT_LEADER);
    set_line(priv);
    set_begin(priv, line, first);
    set_len(priv, 1);
    emit(priv);

    begin = first + 1;
    while (isspace(*begin)) {
        begin++;
    }

    set_type(priv, FOUNTAIN_TOKEN_CENTERED_TEXT_BEGIN);
    /* set_line(priv); */ /* already called */
    set_begin(priv, line, begin);
    set_len(priv, 0);
    emit(priv);

    end = last - 1;
    while (isspace(*end)) {
        end--;
    }
    end++;

    lex_text_with_end(priv, line, begin, end);

    set_type(priv, FOUNTAIN_TOKEN_CENTERED_TEXT_END);
    /* set_line(priv); */ /* already called */
    set_begin(priv, line, end);
    set_len(priv, 0);
    emit(priv);

    set_type(priv, FOUNTAIN_TOKEN_CENTERED_TEXT_TRAILER);
    /* set_line(priv); */ /* already called */
    set_begin(priv, line, last);
    set_len(priv, 1);
    emit(priv);
}

static void
lex_scene_heading(struct priv * priv, const struct fountain_line * line,
        const char * first, const char * last)
{
    const char * scene_number_begin = NULL;
    const char * scene_number_end;
    const char * end = last;

    /*
     * Skip spaces as scene numbers are not required to be at the end of
     * the line.
     */
    while (isspace(*end)) {
        end--;
    }

    if (*end == '#') {
        /* Scene number? */
        scene_number_end = end;
        do {
            end--;
        } while (isalnum(*end) || *end == '.' || *end == '-');
        if (*end == '#') {
            /* Scene number. */
            scene_number_begin = end;

            do {
                end--;
            } while(isspace(*end) && end > first);
        }
    }

    /*
     * end now points to non-whitespace, so increment it to get the
     * exclusive end.
     */
    end++;

    /*
     * Emit the SCENE_HEADING token and the SCENE_NUMBER token, if any.
     */
    set_type(priv, FOUNTAIN_TOKEN_SCENE_HEADING);
    set_line(priv);
    set_begin(priv, line, first);
    set_end(priv, line, end);
    emit(priv);

    if (scene_number_begin) {
        set_type(priv, FOUNTAIN_TOKEN_SCENE_NUMBER_BEGIN);
        /* set_line(priv); */ /* already called */
        set_begin(priv, line, scene_number_begin);
        set_len(priv, 1);
        emit(priv);

        set_type(priv, FOUNTAIN_TOKEN_SCENE_NUMBER);
        /* set_line(priv); */ /* already called */
        set_begin(priv, line, scene_number_begin + 1);
        set_end(priv, line, scene_number_end);
        emit(priv);

        set_type(priv, FOUNTAIN_TOKEN_SCENE_NUMBER_END);
        /* set_line(priv); */ /* already called */
        set_begin(priv, line, scene_number_end);
        set_len(priv, 1);
        emit(priv);
    }
}

#define UINT_LEAST32_SHIFT(c, s) (((uint_least32_t)(c)) << s)
#define TO_UINT_LEAST32(c0, c1, c2, c3) (UINT_LEAST32_SHIFT(c0, 0) \
    | UINT_LEAST32_SHIFT(c1, 8) \
    | UINT_LEAST32_SHIFT(c2, 16) \
    | UINT_LEAST32_SHIFT(c3, 24))

static uint_least32_t
to_uint_least32(const char s[4])
{
    return TO_UINT_LEAST32(s[0], s[1], s[2], s[3]);
}

static int
is_unforced_scene_heading(const char * first)
{
    char s[8];
    memset(s, 0, 8);
    strncpy(s, first, 8);
    for (int i = 0; i < 8; i++) {
        s[i] = tolower(s[i]);
    }

    uint_least32_t i1 = to_uint_least32(s);
    switch (i1) {
        case TO_UINT_LEAST32('i', 'n', 't', ' '):
        case TO_UINT_LEAST32('i', 'n', 't', '.'):
            /* Catches "int./ext " and "int./ext." aswell. */
        case TO_UINT_LEAST32('e', 'x', 't', ' '):
        case TO_UINT_LEAST32('e', 'x', 't', '.'):
        case TO_UINT_LEAST32('e', 's', 't', ' '):
        case TO_UINT_LEAST32('e', 's', 't', '.'):
        case TO_UINT_LEAST32('i', '/', 'e', ' '):
        case TO_UINT_LEAST32('i', '/', 'e', '.'):
            return 1;
        case TO_UINT_LEAST32('i', 'n', 't', '/'):
            ;
            uint_least32_t i2 = to_uint_least32(s+4);
            switch (i2) {
                case TO_UINT_LEAST32('e', 'x', 't', ' '):
                case TO_UINT_LEAST32('e', 'x', 't', '.'):
                    return 1;
                default:
                    return 0;
            }
        default:
            return 0;
    }
}

static bool
lex_possible_character(struct priv * priv,
        const struct fountain_line * line, const char * first,
        const char * last)
{
    const char * dual_dialogue = NULL;
    const char * char_ext_begin = NULL;
    const char * char_ext_end;

    /*
     * The caret must be the *last* character on the line, so check four
     * double dialog here and skip spaces afterwards.
     */
    if (*last == '^') {
        dual_dialogue = last;
        last--;
    }

    /* Skip spaces. */
    while (isspace(*last)) {
        last--;
    }

    /*
     * Handle character extensions. They must be checked for before
     * scanning if the line contains no lowercase characters, since
     * extensions may contain any characters.
     */
    if (*last == ')') {
        char_ext_end = last;
        do {
            last--;
        } while (last >= first && *last != '(');
        if (last < first) {
            /* No opening parenthesis found. Reset last. */
            last = char_ext_end;
        } else {
            char_ext_begin = last;
            if (last > first) {
                /*
                 * It is possible to decrement without a buffer
                 * overflow, since *first is not a space.
                 */
                do {
                    last--;
                } while (isspace(*last));
            } else {
                /*
                 * The line contains only something that is enclosed in
                 * a pair of parentheses, so it cannot be a character.
                 */
                return false;
            }
        }
    }

    if (*first == '@') {
        const char * forcing = first;
        first++;
        first = skip_spaces(first);

        set_type(priv, FOUNTAIN_TOKEN_CHARACTER_FORCING);
        set_line(priv);
        set_begin(priv, line, forcing);
        set_len(priv, 1);
        emit(priv);
    } else {
        /*
         * From the spec.:
         *
         * "Character names must include at least one alphabetical
         * character. "R2D2" works, but "23" does not."
         *
         * This rule is ignored for forced characters, but here it
         * should be respected.
         */
        const char * c;
        for (c = first; c <= last && !isalpha(*c); ++c) {
            // Empty.
        }
        if (c > last) {
            /* No alphabetical character. */
            return false;
        }

        for (; c <= last && !islower(*c); c++) {
            // Empty.
        }
        if (c <= last) {
            /* islower(*c), so it is not a character. */
            return false;
        }
    }

    set_type(priv, FOUNTAIN_TOKEN_CHARACTER);
    set_line(priv);
    set_begin(priv, line, first);
    set_end(priv, line, last + 1);
    emit(priv);

    if (char_ext_begin) {
        set_type(priv, FOUNTAIN_TOKEN_CHARACTER_EXTENSION_BEGIN);
        /* set_line(priv); */ /* already called */
        set_begin(priv, line, char_ext_begin);
        set_len(priv, 1);
        emit(priv);

        set_type(priv, FOUNTAIN_TOKEN_CHARACTER_EXTENSION);
        /* set_line(priv); */ /* already called */
        set_begin(priv, line, char_ext_begin + 1);
        set_end(priv, line, char_ext_end);
        emit(priv);

        set_type(priv, FOUNTAIN_TOKEN_CHARACTER_EXTENSION_END);
        /* set_line(priv); */ /* already called */
        set_begin(priv, line, char_ext_end);
        set_len(priv, 1);
        emit(priv);
    }

    if (dual_dialogue) {
        set_type(priv, FOUNTAIN_TOKEN_DUAL_DIALOGUE);
        /* set_line(priv); */ /* already called */
        set_begin(priv, line, dual_dialogue);
        set_len(priv, 1);
        emit(priv);
    }

    return true;
}

static bool
is_next_line_empty(struct priv * priv)
{
    const struct fountain_line * next = read_line(priv);
    bool next_line_is_empty = next && line_is_empty(next);
    unread_line(priv, next);
    return next_line_is_empty;
}

static void
lex_body_line(struct priv * priv,
        const struct fountain_line * line,
        const char * first,
        bool prev_line_was_empty)
{
    const char * last;
    bool next_line_is_empty = is_next_line_empty(priv);

    /* The line is non-empty so last points to valid memory. */
    last = line->begin + line->len - 1;

    if (*first == '>') {
        if (*last == '<') {
            lex_centered_text(priv, line, first, last);
            return;
        } else {
            /* Forced transition. */
            /* TODO */
        }
    } else if (prev_line_was_empty) {
        /* Scene heading or character? */
        if (next_line_is_empty) {
            /* Not character. Scene heading? */
            if (*first == '.') {
                /* Forced scene heading? */
                const char * forcing = first;
                first++;
                first = skip_spaces(first);

                /*
                 * From the spec.:
                 *
                 * "Note that only a single leading period followed by
                 * an alphanumeric character will force a Scene Heading.
                 * This allows the writer to begin Action and Dialogue
                 * elements with ellipses without worry that they'll be
                 * interpreted as Scene Headings."
                 */
                if (isalnum(*first)) {
                    /* Emit the forcing token. */
                    set_type(priv, FOUNTAIN_TOKEN_SCENE_HEADING_FORCER);
                    set_line(priv);
                    set_begin(priv, line, forcing);
                    set_len(priv, 1);
                    emit(priv);

                    lex_scene_heading(priv, line, first, last);
                    return;
                }
            } else if (is_unforced_scene_heading(first)) {
                lex_scene_heading(priv, line, first, last);
                return;
            }
        } else {
            /* Not scene heading. Character? */
            bool was_character = lex_possible_character(priv, line,
                    first, last);
            if (was_character) {
                return;
            }
        }
    }

    /* TODO */
    /* Action */
    /* Character */
    /* Lyrics */
    /* Transition */
    /* Page break */
    /* Section */
    /* Synopses */
}

static void
lex_body(struct priv * priv)
{
    const struct fountain_line * line;
    bool prev_line_was_empty = false;

    while ((line = read_line(priv))) {
        /*
         * Find the first non-space character. From the spec.:
         *
         * "Leading tabs or spaces in elements other than Action will be
         * ignored."
         */
        const char * first = skip_spaces(line->begin);

        if (line_is_empty(line)) {
            prev_line_was_empty = true;
        } else {
            lex_body_line(priv, line, first, prev_line_was_empty);
            prev_line_was_empty = false;
        }
        rel_line(priv, line);
    }
}

static void
lex_screenplay(struct priv * priv)
{
    lex_title_page(priv);
    lex_body(priv);
}

static void
lex_for_tokens_cb(const struct fountain_token * token, void * data)
{
    struct priv * priv = data;

    if (priv->n_tokens >= priv->n_alloc_tokens) {
        struct fountain_token * new_tokens;
        size_t new_n_alloc_tokens =
            priv->n_alloc_tokens + TOKENS_CHUNK_SIZE;

        new_tokens = realloc(priv->tokens,
                new_n_alloc_tokens * sizeof(*token));
        if (new_tokens == NULL) {
            /*
             * Not enough memory. Sooner or later the lexer will run out
             * of lines and return to the original caller, which will be
             * notified of the memory outage by the error member of the
             * lexer struct.
             */
            priv->lexer->error = FOUNTAIN_LEXER_ERROR_MEMORY;
        }

        priv->tokens = new_tokens;
        priv->n_alloc_tokens = new_n_alloc_tokens;
    }

    memcpy(priv->tokens + priv->n_tokens, token, sizeof(*token));
}

void
fountain_lexer_lex(struct fountain_lexer * lexer)
{
    lexer->error = FOUNTAIN_LEXER_OK;

    validate_lexer(lexer);
    if (lexer->error) {
        return;
    }

    struct priv priv;

    init_priv(&priv, lexer);

    if (lexer->report == FOUNTAIN_LEXER_REPORT_TOKENS) {
        priv.tokens = lexer->tokens;
        priv.n_alloc_tokens = lexer->n_alloc_tokens;
        priv.n_tokens = 0;
        fountain_lexer_callback(lexer, &lex_for_tokens_cb, &priv);
    }

    switch (lexer->source) {
        case FOUNTAIN_LEXER_SOURCE_FILE:
            priv.class = &file_reader_class;
            break;
        case FOUNTAIN_LEXER_SOURCE_STRING:
            /* TODO: string_reader_class */
            break;
        case FOUNTAIN_LEXER_SOURCE_LINES:
            /* TODO: lines_reader_class */
            break;
        case FOUNTAIN_LEXER_SOURCE_INVALID:
            /* This should never happen. */
            break;
    }

    (*priv.class->init)(&priv.reader, lexer);

    lex_screenplay(&priv);

    /*
     * If the lexer struct has been modified, restore it before
     * returning.
     */
    if (lexer->callback == &lex_for_tokens_cb && lexer->data == &priv) {
        fountain_lexer_for_tokens(lexer, priv.tokens,
                priv.n_alloc_tokens);
    }

    deinit_priv(&priv);
}
