/*
 * This file is part of libfountain-lexer.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include <fountain/lexer.h>

static FILE * out;

static void
print_token(const struct fountain_token * token, void * data)
{
    fprintf(out,
            "token: %s (%d)\n"
            " line: %u\n",
            fountain_token_strtype(token->type), token->type,
            token->line);

    if (token->col_end > token->col_begin) {
        fprintf(out,
                " col_begin: %u\n"
                " col_end: %u\n",
                token->col_begin,
                token->col_end);

        fputs(" value: ", out);
        const char * ch = token->begin;
        const char * end = ch + token->col_end - token->col_begin;
        for (; ch < end; ++ch) {
            fputc(*ch, out);
        }
        fputc('\n', out);
    } else {
        fprintf(out, " col: %u\n", token->col_begin);
    }

    return;
}

int
main(int argc, char *argv[])
{
    struct fountain_lexer lexer;
    FILE * file = NULL;
    int exit_status = EXIT_FAILURE;

    /* TODO: allow any file as output */
    out = stdout;

    fountain_lexer_init(&lexer);

    /* TODO: allow any file as input */
    file = fopen("example.fountain", "r");
    if (!file) {
        perror("fopen");
        goto error;
    }

    fountain_lexer_file(&lexer, file);
    fountain_lexer_callback(&lexer, &print_token, NULL);

    fountain_lexer_lex(&lexer);

    exit_status = EXIT_SUCCESS;
error:
    fountain_lexer_deinit(&lexer);
    if (file) {
        fclose(file);
    }
    return exit_status;
}
